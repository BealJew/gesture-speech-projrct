﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;

public class GestureRecognizeManager : MonoBehaviour
{
    public static GestureRecognizeManager Instance { get; private set; }

    public HandModelBase leftHandModel;
    public HandModelBase rightHandModel;
    private GestureRecognize_OneHand leftHand = new GestureRecognize_OneHand();
    private GestureRecognize_OneHand rightHand = new GestureRecognize_OneHand();
    private bool leftCanSee = false;
    private bool rightCanSee = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        leftHandModel.OnBegin += LeftHandIn;
        rightHandModel.OnBegin += RightHandIn;

        leftHandModel.OnFinish += LeftHandIn;
        rightHandModel.OnFinish += RightHandIn;
    }

    void Update()
    {
        //获取手数据
        leftHand.SetHand(leftHandModel.GetLeapHand());
        rightHand.SetHand(rightHandModel.GetLeapHand());
        //更新手状态信息
        leftHand.UpdateStatus(leftCanSee);
        rightHand.UpdateStatus(rightCanSee);

        if (!AnimateManager.Instance.HaveAnimator()) return;
        #region 第一层手势
        if (AnimateManager.Instance.GetStateInfo().IsName(AnimationStateName.IDLE))
        {
            if (leftHand.isYeah)
            {
                EventCenter.GetInstance().EventTrigger(MyEventType.YEAHGESTURE);
            }
            else if(rightHand.isWaving)
            {
                EventCenter.GetInstance().EventTrigger(MyEventType.HELLOW);
            }
            else if(rightHand.indexMoveVel != 0)
            {
                EventCenter.GetInstance().EventTrigger<float>(MyEventType.ROTATEGESTURE, rightHand.indexMoveVel);
            }
        }
        #endregion
    }

    /// <summary>
    /// 捕捉到左手
    /// </summary>
    private void LeftHandIn()
    {
        leftCanSee = true;
    }

    /// <summary>
    ///失去左手
    /// </summary>
    private void LeftHandOut()
    {
        leftCanSee = false;
    }

    private void RightHandIn()
    {
        rightCanSee = true;
    }
    private void RightHandOut()
    {
        rightCanSee = false;
    }
}
