﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPush : MonoBehaviour
{
    public float maxLifeTime = 2f;
    private void OnEnable()
    {
        Invoke("AutoPushInPool", maxLifeTime);
    }

    private void AutoPushInPool()
    {
        PoolManager.GetInstance().PushGObj(this.gameObject.name, this.gameObject);
    }
}
