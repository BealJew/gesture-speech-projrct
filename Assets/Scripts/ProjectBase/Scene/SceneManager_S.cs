﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ScenesManager_S : BaseManager<ScenesManager_S>
{
    /// <summary>
    /// 同步加载场景
    /// </summary>
    /// <param name="name">场景名</param>
    /// <param name="callback">执行的函数</param>
    public void LoadScene(string name, UnityAction callback)
    {
        //同步加载场景
        SceneManager.LoadScene(name);
        callback.Invoke();
    }

    /// <summary>
    /// 提供给外部的异步加载方法
    /// </summary>
    /// <param name="name">场景名</param>
    /// <param name="callback">执行的函数</param>
    public void LoadSceneAsyn(string name, UnityAction callback = null)
    {
        MonoManager.GetInstance().StartCoroutine(ReallyLoadSceneAsyn(name, callback));
    }

    /// <summary>
    /// 异步加载方法
    /// </summary>
    /// <param name="name"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    private IEnumerator ReallyLoadSceneAsyn(string name, UnityAction callback = null)
    {
        yield return new WaitForEndOfFrame();
        float displayProgress = 0f;
        float toProgress = 0f;
        AsyncOperation ao = SceneManager.LoadSceneAsync(name);
        ao.allowSceneActivation = false;
        while (ao.progress < 0.9f)
        {
            toProgress = ao.progress;
            while (displayProgress < toProgress)
            {
                displayProgress += 0.05f;
                EventCenter.GetInstance().EventTrigger(MyEventType.LOADINGSCENE_UPDATE, displayProgress);
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 1f;
        while (displayProgress < toProgress)
        {
            displayProgress += 0.01f;
            if (displayProgress > 1f)
                displayProgress = 1f;
            EventCenter.GetInstance().EventTrigger(MyEventType.LOADINGSCENE_UPDATE, displayProgress);
            yield return new WaitForEndOfFrame();
        }
        ao.allowSceneActivation = true;
        if (!ao.isDone)
            yield return ao.progress;
        callback?.Invoke();
    }
}
